# From https://www.raspberrypi.org/forums/viewtopic.php?f=32&t=88557
# Modified October 2015 to play alternate files. Also createsopens a file 
# to log timestamps and files played for debugging.
# If video is playing cntl-C will halt it and the program will wait for 
# the next trigger.
# If you hit cntrl-C while wating for a trigger it will end the program.
#
# 
#
# Dave Robinson 11/1/15

# Added relay support 
# Dave Robinson  6/22/16


# Made a Fog function that accepts on/off and duration. Never really
# send an off as duration
# is used to turn off the machine.
# Fixed the file open issue (a+ creates the file if it isn't there 
# and appends if it is! So simple!
# Dave Robinson 6/23/16

# Fixed up tabs versus spaces issues reported by newer versions of Python.
# Also added two auxiliary GPIO pins for a four relay baord. However their 
# function have not been added yet. 
# Dave Robinson 7/20/19
#
# Added  Aux 1 to turn on  while movie 1 is playing.
# Added  Aux 2 to turn on  while movie 2 is playing.
# Changed movie names to movie1.mp4 and movie2.mpe to make it mode generic.
# Tested on a Rasberry Pi Zero July 25th 2019.
# Tested again with Python3 and had to correct tab/spaces issues.
# Passed on Pi Zero with Python and Python3 July 29th 2019.

import RPi.GPIO as GPIO
import time
import os
import random
from random import randrange



PIR = 18	#  Set GPIO 18 as input for PIR trigger
POWER = 24	#  Set GPIO 17 as output for relay.
Pirate = 23	# Set GPIO 23 as output for Pirate
Aux1 = 20	# Aux pins for Instructable.
Aux2 = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR, GPIO.IN)
GPIO.setup(POWER, GPIO.OUT)
GPIO.setup(Pirate, GPIO.OUT)
GPIO.setup(Aux1, GPIO.OUT)
GPIO.setup(Aux2, GPIO.OUT)

GPIO.output(Pirate, 0)	# Setting everything off = 0; 1 = on
GPIO.output(POWER, 0)
GPIO.output(Aux1, 0)
GPIO.output(Aux2,0)

Loop_count = 10
File_to_play = 1				     # Flag to alternate files to be played



def MyLog(logData):                                  # My logging function
        
        
    log.write(time.asctime(time.localtime(time.time())))  # Add time stamp
    log.write(logData)  # Add padded in text - the current playing/ending file
    log.flush()  # Needed to make sure file gets updated
# End of Log function
def Pirate_Talk():

            GPIO.output(Pirate, 1)
            time.sleep(1)
            GPIO.output(Pirate, 0)
            MyLog(" Pirate Talking!\n")
# End of Pirate

def Fog(sw_mode, Dur):  # Fog function turns on fog machine for passed in "Dur"
                        # Turns off fog machine after "Dur" seconds. 

    if sw_mode == 1:
        FogText = " Fog Machine on for " + str(Dur) + " seconds.\n"
        MyLog(FogText)
        GPIO.output(POWER, sw_mode)
        time.sleep(Dur)
        GPIO.output(POWER, 0)
    if sw_mode == 0:
        MyLog(" Fog machine off.\n")
        GPIO.output(POWER, sw_mode)
# End of Fog function

os.system("clear")                                   # Clear Screen   
os.system("setterm -cursor off")                     # Hide Cursor  
log = open("song.log", "a+")			     # Open a file for logging. 

log.seek(0, 2)  # This will append new entries

HeaderText = "Starting Log at " + time.asctime(time.localtime(time.time())) + "\n"
log.write(HeaderText)
while True:  # Looping until press Ctrl-C to break
	if (GPIO.input(PIR) == True):                        # Check PIR trigger
		Pirate_Talk()
		time.sleep(1)
		Fog(1, 5)                                # Call for fog!!
        if (File_to_play == 1):
            MyLog(" Playing Movie 1.\n")
            GPIO.output(Aux1, 1)
            os.system("omxplayer movie1.mp4 -b > /dev/null")
# Python shell method. Makes sure console data  goes  to dev/null while playing  video
            MyLog(" Movie 1 ended.\n")
            Fog(1, 10)
            File_to_play = 2                           # Flip to next fil
            GPIO.output(Aux1, 0)
            while (GPIO.input(PIR) == True): 
                time.sleep(.2)
		else: 
            MyLog(" Playing Movie 2\n")
            GPIO.output(Aux2, 1)
            os.system("omxplayer movie2.mp4 -b > /dev/null ")         
            MyLog(" Movie 2 ended.\n")
            GPIO.output(Aux2, 0)
            Fog(1, 10)
            File_to_play = 1                           # Flip to first file
            while (GPIO.input(PIR) == True): 
                time.sleep(.2)  
		while (GPIO.input(PIR) == True):
			time.sleep(.2)                             # Loop until PIR is cleared




# End of Program.