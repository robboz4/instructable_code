# Halloween Controller

Code needed for my Halloween Instructable: https://www.instructables.com/id/Automated-Singing-Pumpkins/

 There are two versions:
 
    1) Halloween_Control.py - this has 4 relay support and two movies support and takes an option parameter '-r'. This parameter is 
    that turn on immediatley the program runs. By adding the parameter the program will turn off the relays when it's starts and run
    normally. For this case  the run string is "sudo python Halloween_Control.py -r"
    
        The relays are  1) a one second pulse that activated the pirate inthe instructable.
        
                        2) The fog machine trigger with  5 and 10 second bursts at the beginning and end of the movie.
                        
                        3) An output that is operated at the begining of movie one and turned off after movie one ends.
                        
                        4) An output that is operated at the begining of movie two and turned off after movie two  ends.
                    
        For generic reasons I have made the movie names - movie1.mp4, and movie2.mp4. This is you can add different movies by changing the names. 
        If you only have one movie you can copy it to movie2.mp4 so the code will play the same movie without giving an error.
        
        GPIO pins ( BCM naming/numbering mode) for this code are:
            PIR = 18 
            Relay 1 = 23 - pirate
            Relay 2 = 24 - fog
            Rleay 3 = 20 - movie 1 effect
            Relay 4 = 21 - movie 2 effect
    2) Halloween-control-instrucable.py - simple PIR triggered to play the original movies. PIR is on GPIO pin 18. No relay outputs. Needs to two
    movie files: one called Singing_Pumpkins.mp4 and Thriller-5Frames.mp4. See instructable for movies.
    
    